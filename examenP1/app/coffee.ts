export class Coffee {
  constructor(
    public id:number,
    public name:string,
    public email:string,
    public tel:number) { }
}