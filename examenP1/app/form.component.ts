import {Component} from 'angular2/core';
import {NgForm}    from 'angular2/common';
import { Coffee }    from './coffee';
import {Http} from 'angular2/http';
import { Inject} from 'angular2/core';
import 'rxjs/add/operator/map';


@Component({
  selector: 'my-form',
  templateUrl: 'app/form.html'
})

export class FormComponent {
  constructor( @Inject(Http) private http: Http) {
  }
  model = new Coffee(18, '', '', '');

  /*if(this.model.tel.length < 8)
  {

  }*/

  Envio = function(){
  	if (this.model.name == '' || this.model.email == '' || this.model.tel == '')
  	{
  		alert('Todos los campos son requeridos');
  	}
  	else
  	{
  		/*this.http.post("http://default-environment.sxpxvycigr.us-west-2.elasticbeanstalk.com/users", {
            name: this.model.name,
            email: this.model.email,
            
          }); */

            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Access-Control-Allow-Origin', 'http://localhost:3000');

      var creds = "name=" + this.model.name + "&email=" + this.model.email;

      this.http.post('http://default-environment.sxpxvycigr.us-west-2.elasticbeanstalk.com/users', creds, {
        headers: headers
      })
      .map(res => res.json())
      .subscribe(
        data => console.log(data),
        err => console.log(err),
        () => console.log('Registry Complete')
      );
  	
  }

  checkLength = function(Number){
      if (Number.length < 8)
      {
        return false;
      }
      else
      {
        return true;
      }
   }
  }
}