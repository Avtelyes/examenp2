System.register(['angular2/core', './coffee', 'angular2/http', 'rxjs/add/operator/map'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, coffee_1, http_1, core_2;
    var FormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
                core_2 = core_1_1;
            },
            function (coffee_1_1) {
                coffee_1 = coffee_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            FormComponent = (function () {
                function FormComponent(http) {
                    this.http = http;
                    this.model = new coffee_1.Coffee(18, '', '', '');
                    /*if(this.model.tel.length < 8)
                    {
                  
                    }*/
                    this.Envio = function () {
                        if (this.model.name == '' || this.model.email == '' || this.model.tel == '') {
                            alert('Todos los campos son requeridos');
                        }
                        else {
                            /*this.http.post("http://default-environment.sxpxvycigr.us-west-2.elasticbeanstalk.com/users", {
                                name: this.model.name,
                                email: this.model.email,
                                
                              }); */
                            var headers = new Headers();
                            headers.append('Content-Type', 'application/x-www-form-urlencoded');
                            headers.append('Access-Control-Allow-Origin', 'http://localhost:3000');
                            var creds = "name=" + this.model.name + "&email=" + this.model.email;
                            this.http.post('http://default-environment.sxpxvycigr.us-west-2.elasticbeanstalk.com/users', creds, {
                                headers: headers
                            })
                                .map(function (res) { return res.json(); })
                                .subscribe(function (data) { return console.log(data); }, function (err) { return console.log(err); }, function () { return console.log('Registry Complete'); });
                        }
                        checkLength = function (Number) {
                            if (Number.length < 8) {
                                return false;
                            }
                            else {
                                return true;
                            }
                        };
                    };
                }
                FormComponent = __decorate([
                    core_1.Component({
                        selector: 'my-form',
                        templateUrl: 'app/form.html'
                    }),
                    __param(0, core_2.Inject(http_1.Http)), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], FormComponent);
                return FormComponent;
            })();
            exports_1("FormComponent", FormComponent);
        }
    }
});
//# sourceMappingURL=form.component.js.map