System.register([], function(exports_1) {
    var Coffee;
    return {
        setters:[],
        execute: function() {
            Coffee = (function () {
                function Coffee(id, name, email, tel) {
                    this.id = id;
                    this.name = name;
                    this.email = email;
                    this.tel = tel;
                }
                return Coffee;
            })();
            exports_1("Coffee", Coffee);
        }
    }
});
//# sourceMappingURL=coffee.js.map