import {Component}         from 'angular2/core';
import {FormComponent} from './form.component'


@Component({
  selector: 'my-app',
  template: '<my-form></my-form>',
  directives: [FormComponent]
})
export class AppComponent { }
